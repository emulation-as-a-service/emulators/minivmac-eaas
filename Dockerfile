from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="vmacmini"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"


copy minivmac /minivmac
workdir minivmac
run gcc setup/tool.c -o setup_t
run ./setup_t -t lx64 -api sdl > setup.sh
run bash setup.sh
run make
copy vmac.rom /minivmac/vMac.ROM
